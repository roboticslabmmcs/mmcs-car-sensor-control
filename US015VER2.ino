#include <NewPing.h>
#include <String.h>
//1
#define US_LONG_US015_1_1 30//триггер   боковой левый
#define US_LONG_US015_1_2 31//эхо
//2
#define Parktronic_1_1 33//триггер фронтальный левый
#define Parktronic_1_2 32//эхо
//3
#define US_LONG_US015_3_1 34//фронтальный центр
#define US_LONG_US015_3_2 35//
//4
#define Parktronic_2_1 29//фронтальный правый
#define Parktronic_2_2 28
//5
#define US_LONG_US015_5_1 26//боковой правый
#define US_LONG_US015_5_2 27

//////////////////////////////////////////////////////////////////////////////////////

//Максимальная дистанция опроса
#define MAX_DISTANCE_US015 500//для передних
#define MAX_DISTANCE_US015b 300//для боковых
#define MAX_DISTANCE_Parktronic 200//парктроники


//Максимальная дистанция срабатывания(0/1)
#define MAX_DETECT_US015 150
#define MAX_DETECT_US015b 100
#define MAX_DETECT_Parktronic 80

#define MAX_COMMAND_LENGTH 20

NewPing sensorUS015_1(US_LONG_US015_1_1, US_LONG_US015_1_2, MAX_DISTANCE_US015b); //объект для US-015_1(боковой левый)
NewPing sensorParktronic_1(Parktronic_1_1, Parktronic_1_2, MAX_DISTANCE_Parktronic); //объект для Parktronic_1(фронтальный левый)
NewPing sensorUS015_3(US_LONG_US015_3_1, US_LONG_US015_3_2, MAX_DISTANCE_US015); //объект для US-015_3(фронтальный центр)
NewPing sensorParktronic_2(Parktronic_2_1, Parktronic_2_2,  MAX_DISTANCE_Parktronic); //объект для Parktronic_2(фронтальный правый)
NewPing sensorUS015_5(US_LONG_US015_5_1, US_LONG_US015_5_2, MAX_DISTANCE_US015b); //объект для US-015_5(боковой правый)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int all_area[8];

char command_buf[MAX_COMMAND_LENGTH];

void setup() {
  Serial1.begin(19200);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                        Функции получения расстояния
/////////////////////////////////////////////////////////////////////////////////////////////////////////
int get_distance_sensor(int sonar_number)
{
  switch (sonar_number)
  {
    case 1:
      {
        int _length = sensorUS015_1.convert_cm(sensorUS015_1.ping_median(5));
        if (_length == 0)
          _length = MAX_DISTANCE_US015b;
        return _length;
      } break;

    case 2:
      {
        int _length = sensorParktronic_1.convert_cm(sensorParktronic_1.ping_median(5));
        if (_length == 0)
          _length = MAX_DISTANCE_Parktronic;
        return _length;
      } break;

    case 3:
      {
        int _length = sensorUS015_3.convert_cm(sensorUS015_3.ping_median(5));
        if (_length == 0)
          _length = MAX_DISTANCE_US015;
        return _length;
      } break;

    case 4:
      {
        int _length = sensorParktronic_2.convert_cm(sensorParktronic_2.ping_median(5));
        if (_length == 0)
          _length = MAX_DISTANCE_Parktronic;
        return _length;
      } break;

    case 5:
      {
        int _length = sensorUS015_5.convert_cm(sensorUS015_5.ping_median(5));
        if (_length == 0)
          _length = MAX_DISTANCE_US015b;
        return _length;
      } break;

  }
}




///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                        Функции комплексного сканирования окружения
//////////////////////////////////////////////////////////////////////////////////////////////////////

//Сканирование зоны перед машиной(сканирование,слево на право)
int*get_distance_fronline()
{
  int A_fr[5];
  A_fr[0] = get_distance_sensor(2);
  A_fr[1] = get_distance_sensor(3);
  A_fr[2] = get_distance_sensor(4);
  return A_fr;
}


//Сканирование по бокам(левый потом правый)
int*get_distance_angle()
{
  int A_ang[5];
  A_ang[0] = get_distance_sensor(1);
  A_ang[1] = get_distance_sensor(5);
  return A_ang;
}
//Сканирование всей зоны вокруг машины(сканирования,слево на право;сначала спереди,потом сзади)
int*scan_all_area()
{
  
  all_area[0] = get_distance_sensor(1);
  all_area[1] = get_distance_sensor(2);
  all_area[2] = get_distance_sensor(3);
  all_area[3] = get_distance_sensor(4);
  all_area[4] = get_distance_sensor(5);
  return  all_area;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Печать массива из scan_all_area в serial
void serial_all_area()
{
  for (int i = 0; i < 5; ++i)
  {
    Serial1.print(scan_all_area()[i]);
    Serial1.print(" ");
  }
  Serial1.println("");
}

String GetCommand() // получение команды из COM-порта, если команды нет, то возвращается пустая строка
{
  int inputCount = Serial1.available();
  if (inputCount > 0)
  {
    int totalByte = Serial1.readBytesUntil('\n', command_buf, MAX_COMMAND_LENGTH);
    String command = command_buf;
    command = command.substring(0, totalByte);
    command.replace("\r", "");
    command.replace("\n", "");
    return command;
  }
  else
    return "";
}
void CheckCommand()
{
  Serial1.flush();
  String command_name = GetCommand();
  if (command_name.length() > 0)
  {
    if (command_name == "STATUS")
      Serial1.println("ARDUINO SONAR");
    else if (command_name == "GET")
    {
      String stat = "D";
      int*a = scan_all_area();
      if (a[0] <= MAX_DETECT_US015b) stat = stat + "1";
      else  stat = stat + "0";
      if (a[1] <= MAX_DETECT_Parktronic) stat = stat + "1";
      else  stat = stat + "0";
      if (a[2] <= MAX_DETECT_US015) stat = stat + "1";
      else  stat = stat + "0";
      if (a[3] <=MAX_DETECT_Parktronic) stat = stat + "1";
      else  stat = stat + "0";
      if (a[4] <= MAX_DETECT_US015b) stat = stat + "1";
      else  stat = stat + "0";
      Serial1.println(stat);
    }
  }

}
void loop()
{
      String stat = "D";
      int*a = scan_all_area();
      if (a[0] <= MAX_DETECT_US015b) stat = stat + "1";
      else  stat = stat + "0";
      if (a[1] <= MAX_DETECT_Parktronic) stat = stat + "1";
      else  stat = stat + "0";
      if (a[2] <= MAX_DETECT_US015) stat = stat + "1";
      else  stat = stat + "0";
      if (a[3] <=MAX_DETECT_Parktronic) stat = stat + "1";
      else  stat = stat + "0";
      if (a[4] <= MAX_DETECT_US015b) stat = stat + "1";
      else  stat = stat + "0";
      
      stat=stat+"/";
      stat=stat+a[0];
      stat=stat+","+a[1];
      stat=stat+","+a[2];
      stat=stat+","+a[3];
      stat=stat+","+a[4];

    
      Serial1.println(stat);
      delay(100);
}
